# Utils

Useful files for multiple projects

### .gitignore
Gitignore file for python scripts (based on https://github.com/github/gitignore)

### logging.config
Config file for python logging lib
