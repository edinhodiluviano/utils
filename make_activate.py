#!/usr/bin/env python3

import os


FILENAME = 'activate'
COMMAND_UNSET = '    unset $(cut -d "=" -f 1 .env)'
COMMAND_SET = 'export $(cat .env)'


def main():
    path = os.environ['VIRTUAL_ENV']
    filepath = os.path.join(path, 'bin', FILENAME)
    with open(filepath, 'r') as f:
         conts = f.read()
    conts = conts.split('\n')
    # add set command:
    for i in range(3):
        conts.append('')
    conts.append(COMMAND_SET)
    # add unset command:
    conts.insert(4, COMMAND_UNSET)
    conts.insert(5, '')
    # rebuild file contents and create new file
    conts = '\n'.join(conts) + '\n'
    filepath = os.path.split(path)[0]
    filepath = os.path.join(filepath, FILENAME)
    with open(filepath, 'w') as f:
        f.write(conts)


if __name__ == '__main__':
    main()
